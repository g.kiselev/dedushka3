<?php

header('Content-Type: text/html; charset=UTF-8');


if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  
  if (!empty($_GET['save'])) {
   
    print('Thank you , the results are saved.');
  }

  include('form.php');
 
  exit();
}

$errors = FALSE;
if (empty($_POST['fio'])) {
  print('Fill in the name.<br/>');
  $errors = TRUE;
}

if (empty($_POST['email'])) {
    print('Fill in your email.<br/>');
    $errors = TRUE;
}

if (empty($_POST['bio'])) {
    print('Fill in your biography.<br/>');
    $errors = TRUE;
}

if (empty($_POST['check-1'])) {
    print('Read the contract.<br/>');
    $errors = TRUE;
}

if (empty($_POST['field-name-2'])) {
    print('Select a superpower.<br/>');
    $errors = TRUE;
}

if (strlen($_POST['fio'])<3) {
    print('the name is too small.<br/>');
    $errors = TRUE;
}

if (strlen($_POST['fio'])>12) {
    print('the name is too big.<br/>');
    $errors = TRUE;
}

if (strlen($_POST['bio'])<20) {
    print('the biography should be bigger.<br/>');
    $errors = TRUE;
}


if ($errors) {
  
  exit();
}


$user = 'u16654';
$pass = '8728536';
$db = new PDO('mysql:host=localhost;dbname=u16654', $user, $pass, array(PDO::ATTR_PERSISTENT => true));


try {
  $stmt = $db->prepare("INSERT INTO application SET name = ?, mail = ?, dat = ?, gender = ?, limbs = ?, superpowers = ?, biography = ?");

    
  $stmt->execute(array($_POST['fio'], $_POST['email'],$_POST['date'],$_POST['radio-group-1'],$_POST['radio-group-2'],
       $_POST['field-name-2'],$_POST['bio']));
    
 

}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  exit();
}

header('Location: ?save=1');