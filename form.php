<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <title>Form</title>
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,700&amp;subset=cyrillic-ext">
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <div class="main-container ">
  
		
    <div class="form">
      <form action="" method="POST">
        <div id="form-1">
          <label>
           Name :<br >
            <input name="fio" placeholder="Name" />
          </label><br>
        
          <label>
            email:<br >
            <input name="email" placeholder="name@mail.ru" type="email" />
          </label><br>
          
   
          <label id="date">
          Date<br>
            <input name="date" value="2000-06-25" type="date"/>
          
      </label>
        </div>

        <div id="form-2">
       	  <div id="form-2-container">
            <label> Gender:<br>
              <input type="radio" checked="checked" name="radio-group-1" value="male" />
              Male
            </label><br>

            <label>
              <input type="radio" name="radio-group-1" value="female" />
               Female
            </label><br>
            <br>

           Number of limbs:<br>
            <label>
              <input type="radio" name="radio-group-2" value="1" />
              1
            </label><br>

            <label>
              <input type="radio" name="radio-group-2" value="2" />
              2
            </label><br>

            <label>
              <input type="radio" name="radio-group-2" value="3" />
              3
            </label><br>

            <label>
              <input type="radio" checked="checked" name="radio-group-2" value="4" />
              4
            </label><br>

            <label>
              <input type="radio" name="radio-group-2" value=">4" />
              >4
            </label><br>
            <br>
          </div>
        </div>
        <div id="form-3">
          <div id="form-3-container">
            <div id="super">  
              <label>
                Your superpowers:<br>
                <select name="field-name-2" multiple="multiple">
                  <option value="Telepathy">Telepathy</option>
                  <option value="Telekinesis" >Telekinesis</option>
                  <option value="Levitation" >Levitation</option>
                  <option value="Absolute memory" >Absolute memory</option>
                </select>
              </label><br>
           </div>

            <div id="Bio">
              <label>
                Biography:<br >
                <textarea name="bio"></textarea>
              </label><br >
            </div>
              <label>
                <input type="checkbox" checked="checked" name="check-1" />
                I have read the contract
              </label><br >
        
              <input type="submit" value="Submit" />
         </div>
        </div>
      </form>
    </div>
     <!-- <footer>
      	<div >
      		<div id="footer">
      			<h2>(c)Глеб Киселёв 2019</h2>
      		</div>
      	</div>
      </footer>-->
			            
	</div>
		
	</body>

</html>